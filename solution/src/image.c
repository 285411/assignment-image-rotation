#include "image.h"

struct image image_create( size_t  width, size_t  height){
	struct image result = {0};
	result.width = width;
	result.height = height;
	return result;
}

void image_destroy(struct image* img){
	free(img -> data);
}   
