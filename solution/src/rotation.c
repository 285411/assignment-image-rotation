#include "rotation.h"

static struct image rotate_clockwise(struct image* result, const struct image* source){
	uint64_t x;
	uint64_t y;
	uint64_t pixel_amount = (source -> height) * (source -> width);
	struct pixel* rotated_pixels = malloc(sizeof(struct pixel) * pixel_amount);
	struct pixel* original_pixels = source -> data;
	for (size_t i = 0; i < pixel_amount; i++) {
		x = (source -> height) - 1 - i / (source -> width);
		y = i % (source -> width);
		rotated_pixels[y * (source -> height) + x] = original_pixels[i];
	}
	result -> data = rotated_pixels;
	return *result;
}

struct image rotate( const struct image source) {
	struct image result = image_create(source.height, source.width);
	return rotate_clockwise(&result, &source);
}
