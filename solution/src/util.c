#include "util.h"

 uint32_t padding(uint32_t width) {
     
    return 4 - ((width * sizeof(struct pixel)) % 4);
}
